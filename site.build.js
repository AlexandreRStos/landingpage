const fse = require('fs-extra')
// const sass = require('node-sass')

const buildPath = './build'
const srcPath = './src'

// empty build folder
fse.emptyDirSync(buildPath)

// copy folder img and audio for build
fse.copy(`./${srcPath}/assets`, `./${buildPath}/assets`, err => {
  if (err) return console.error(err)
  console.log(`Copy folder assets with sucess!`)
})

// sass.renderSync({
//   file: './src/style/sass/main.scss',
//   outFile: './src/style/css/main.comp.css'
// })
