window.onscroll = () => stickyMenu() // chamar função ao move o scroll

const sticky = '104'
const $navigation = document.querySelector('.topbar')
const $navbar = document.querySelector('.nav-bar')

function stickyMenu () {
  if (window.pageYOffset >= sticky) { // => comparar se o scroll é maior que o topMenu
    $navigation.classList.add('--fixed') // => add class para fixar menu top
    $navbar.classList.add('--fixed') // => add class para fixar menu top
  } else {
    $navigation.classList.remove('--fixed')
    $navbar.classList.remove('--fixed')
  }
}
