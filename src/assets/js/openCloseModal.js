const $overlayLinks = document.querySelectorAll('.overlay > .link')
const $modalProjetos = document.querySelectorAll('.modal-projetos')
const $btnCloses = document.querySelectorAll('.close')

$overlayLinks.forEach(($overlayLink, index) => {
  $overlayLink.addEventListener('click', (e) => {
    e.preventDefault()
    $modalProjetos[index].classList.add('open')
  })
})

$btnCloses.forEach(($close, index) => {
  $close.addEventListener('click', () => {
    $modalProjetos[index].classList.remove('open')
  })
})