const $navLinks = document.querySelectorAll('.top-nav a')

$navLinks.forEach(link => {
  link.addEventListener('click', unchecked)
})

function unchecked () {
  const $checkbox = document.querySelector('.top-nav__input')
  $checkbox.checked = false
}
